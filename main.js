/**
 *
 * Plan:
 *
 * First: on zoom, initially just stretch mandel brot to fit canvas size. Will appear pixelated.
 *              then in background have webworker rendering the precise set!
 *
 * Second: have various webworkers computing a block of mandelbrot, then all return data and it is displayed.
 *
 * Third:  sort out all compatibility issues with firefox/safari/ie. Make it work on all platforms
 *
 * Created by Thomas on 1/6/2015.
 */

//************************HTML elements!***********************//
var canvas = document.getElementById("mandelbrot"); //properties set in HTML.
var ctx = canvas.getContext("2d"); //only valid

//Options Pane
var inputIterations = document.getElementById("inputIterations");
var inputEscapeRadius = document.getElementById("inputEscapeRadius");
var labelZoom = document.getElementById("textZoom");
var optionsColors = document.getElementById("optionsColor");
//************************end of elements**********************//


//****************CONSTANTS********************//
var numWorkers = 8; //how many workers are computing mandelbrot at various levels

//************Globals**************************//
var myWorkers = [];

var mandelPos = {
    x:  0,
    y:  0,
    zoom : 1,
    xoffset : -300,
    yoffset : 0,
    pixels : 0,
    escape : 10,
    iterations : 200
};

function drawMandelbrot ()
{
    var blankImg = ctx.getImageData(0,0,canvas.width,canvas.height);
    var img = [];
    var workerMessage = [];


    //since each worker will be constructing their own image. We replicate the base canvas
    // to ensure thread isolation
    for (var p = 0; p < numWorkers; p++)
        img[p] = blankImg;

    var i = 0;
    var prevId = 1;

    while (i < numWorkers)
    {
        //start new webworker. save to array
        //Webworkers cannot access the document object, thus we will just pass the array and then draw returned data!
        myWorkers[i] = new Worker("mandelbrot.js");

        //worker parametors for mandelbrot generation
        workerMessage[i] = {
            img             :   img[i],
            zoom            :   mandelPos.zoom,
            xoffset         :   mandelPos.xoffset,
            yoffset         :   mandelPos.yoffset,
            numIterations   :   (mandelPos.iterations/numWorkers)*(1+i),
            width           :   canvas.width,
            height          :   canvas.height,
            workerId        :   (i + 1),
            escape          :   mandelPos.escape,
            color           :   optionsColors.value
        };
        myWorkers[i].postMessage(workerMessage[i]);

        //wait for completion response and write to canvas when complete!
        myWorkers[i].addEventListener('message',function(e) {
            if (e.data.type == 'debug')
            {
                console.log(e.data.msg);
            }
            else {
                //sometimes a more precise worker finishes first
                if(e.data.workerId > prevId)
                {
                    mandelPos.pixels = e.data.img;
                    ctx.putImageData(mandelPos.pixels,0,0);
                    prevId = e.data.workerId;
                }
                if(e.data.workerId == numWorkers)
                {
                    myWorkers.forEach(killWorker);
                }
            }

        },false);
        i++;
    }
}

//standard mouse position function call on event
function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    //temporary offsets, do not reflect complex plane
    mandelPos.x = evt.clientX - rect.left;
    mandelPos.y = evt.clientY - rect.top;

}

function mouseWheelHandler(evt) {
    //first kill all workers! No need in calculating old mandelbrots!
    myWorkers.forEach(killWorker);
    var delta = Math.max(-1, Math.min(1,(evt.wheelDelta || -evt.detail)));

    mandelPos.xoffset += (mandelPos.x - canvas.width/2) *(1.0/mandelPos.zoom);
    mandelPos.yoffset += (mandelPos.y - canvas.height/2) * (1.0/mandelPos.zoom);

    if (delta > 0)
        mandelPos.zoom *= 2;
    else
        mandelPos.zoom /= 2;

    labelZoom.textContent = mandelPos.zoom.toExponential(2);

    drawMandelbrot();
}


function killWorker(element,index,array)
{
    //destroys each worker.
    element.terminate();
    if (index == numWorkers)
    {
        array = [];
    }
}

(function() {
    myWorkers.forEach(killWorker);
    // resize the canvas to fill browser window dynamically
    window.addEventListener('resize', resizeCanvas, false);

    function resizeCanvas() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        drawMandelbrot();
    }
    resizeCanvas();
})();

function mousePressHandler(evt)
{
    myWorkers.forEach(killWorker);

    if (evt.which == 1) {
        mandelPos.xoffset += (mandelPos.x - canvas.width / 2) * (1.0 / mandelPos.zoom);
        mandelPos.yoffset += (mandelPos.y - canvas.height / 2) * (1.0 / mandelPos.zoom);
        drawMandelbrot();
    }
}

function resetMandelbrot ()
{
    myWorkers.forEach(killWorker);

    mandelPos = {
        x:  0,
        y:  0,
        zoom : 1,
        xoffset : -300,
        yoffset : 0,
        pixels : 0,
        escape : 10,
        iterations : 200
    };

    labelZoom.textContent = mandelPos.zoom.toExponential();

    drawMandelbrot();
}

function main ()
{
    drawMandelbrot();

    //attach mouse position listener
    canvas.addEventListener('mousemove', function(evt) {
        getMousePos(canvas, evt);
    }, false);

    //attach mouse click listener for click to center
    canvas.addEventListener('mousedown', mousePressHandler,false);
    //mouse scroll listener for zoom
    canvas.addEventListener('mousewheel',mouseWheelHandler,false);
    //button listeners
    document.getElementById("buttonReset").addEventListener("click",resetMandelbrot,false);
    document.getElementById("buttonRedraw").addEventListener("click",drawMandelbrot,false);
    //text box change listeners
    inputEscapeRadius.addEventListener("change",function(){
        mandelPos.escape = inputEscapeRadius.value;
    },false);
    inputIterations.addEventListener("change",function(){
        mandelPos.iterations = inputIterations.value;
    },false)



}

main();

