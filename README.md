# Javascript Mandelbrot Set #

Mandelbrot set generator written in javascript utilizing webworkers and HTML 5 canvas! 

## Author ##
written by Thomas Davies, www.thomasrdavies.com
## Summary ##

### Mathematics ###
The mandelbrot set is the set of values of c on the complex plane that remain bounded under successive iteration of z(n+1) = z^2(n) + c. May sound complicated but we it really just the sequence [c, c^2 + c, (c^2 + c)^2 ...] which does not approach infinity. To actually generate the mandelbrot set on a canvas, we scan through each pixel of 
	the canvas and iterate it through z trying to detect when it tends to infinity! 

### How To Use? ###
The instructions for usage can be found by clicking "how to navigate?" in the control panel. If you're too lazy to do that here's the instructions.
 
* **Pan**- you can navigate the mandelbrot by simply clicking where you are interested

* **Zoom**- use the scroll wheel to get a closer look.

 * **Iterations and Escape Radius**- both of these parameters can be altered (mouseover for spinner)

* **Hint**- if the screen flashes all black when computing, try increasing the number of iterations!Also...go fullscreen for the best experience(F11 on most browsers).

### Why is this at all interesting? ###
If your canvas size is 1600x900 pixels then you have 1440000 pixels that need to be iterated for generation of the set. If the max number of iterations is 500 then we are looping the core 	iteration function 720 million times just to populate your canvas! Mandelbrot set is an amazing optimization program! My first mandelbrot set was programmed in Turing and took over 30 seconds to generate the set with no zoom at 800x800 resolution. Now by utilizing web workers and some resource saving techniques the runtime of that same task is around 0.3 of a second! By exploring you can come across some truly amazing sights that would have taken months to see just 10 years ago! 

If you still aren't at all interested then at least you can appreciate the beauty of the mathematics involved in the mandelbrot set. Just point and zoom and you will stumble on some of the most amazing sights math can offer you. For the ultimate experience, view in full-screen mode and you can save 	the sights you see by downloading the image! 

### Coloring The Set ###
Traditionally when generating the mandelbrot set the color of each pixel is determined by the iteration count at which the escape radius was reached. In javascript pixel color is described using the RGBA system, so before a pixel can be changed the iteration count is first converted by converting from HSV to RGBA. 

If we stopped here and generated the set there would be visible "bands" of color throughout the set. To reduce the visible banding the smooth coloring algorithm outlined by Linas Vepstas (linas.org) was used. Each color is normalized to reduce the color difference between adjacent pixels.

## Optimizations ##
### Core Iterator ###
The core iterator is the biggest resource consumer and the main focus of webworkers. Since this function is iterated up to 720 million times per generation it is extremely important that it is as fast as can be! The most basic optimizations
	are mathematical simplifications and estimations to reduce the number of operations needed per iteration. The base function iterated is f = z^2 + c which in full is,

```
#!math

     z = (x+yi)^2 + a + bi 
     z = xx - yy + 2xyi + a + bi
     z = (xx - yy + a) + i(2xy + b)
```
The exit point for each iteration is when the length of our complex vector is longer than the escape radius defined. The exit condition is then 

```
#!math

sqrt(xx+yy) > escape_radius
```
However square roots are extremely slow operations in C++ so the exit condition can be rearranged to be 

```
#!math
xx+yy > escape_radius^2
```
which drastically speeds up the comparison.

### Web Workers ###
One of the new features of HTML5 used in this project are webworkers. We programs have been traditionally limited as they have never had threading capability, and any background calculations caused delays in the main interface. Now with webworkers the classic "The webpage has stopped responding" pop-up has finally started to disappear! 

For this mandelbrot set, webworkers are used to generate more and more precise versions giving a "gradual focusing" effect. This method was chosen to allow users to continuously interact with the set as opposed to waiting for complete generation. 
Eight web workers are used and each are given a job with successively higher iteration count. As each worker completes it's	task they then place their pixel results in the canvas memory. 

In the upcoming release webworkers will also be used to improve execution time by simultaneously working on multiple sections	of each mandelbrot generation. Each webworker will be responsible for a row of pixels on the screen and will be given a new job once complete. Another optimization technique in testing are to group pixels in "blocks" which all receive the same color, the block size is then decreased and the mandelbrot is re-calculated. 

## Dependencies ##
The only dependency is jQuery UI for a modal popup. But without it the program will still operate you just won't be able to click "how to navigate?" link.