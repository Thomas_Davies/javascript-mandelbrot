/**
 * Created by Thomas on 1/3/2015.
 */


/*
Background Info:
The mandelbrot set contains all numbers on the complex plane for which successive
    zero iteration does not tend to infinity.

    Let f = z^2 + c
    where z = x + yi
    and c = a + bi

    simplifying,

    f = (x + yi)^2 + a + bi
    f = xx - yy + 2xyi + a + bi
    f = (xx - yy + a) + i(2xy + b)
    f = R + iI

    Too actually draw the mandelbrot set we can use a mapping function which correlates pixel location to
     a point on the complex plane for

 */

//the main iteration function. Biggest resource consumer! (main focus for workers!)
function coreIterator(a,b,maxIterations,escapeRadius) {
    var x = 0.0;
    var y = 0.0;
    var xx = x*x;
    var yy = y*y;
    var xy = x*y;
    var i = 1;

    //two cases for exit.
    //      1. iterations reached max
    //      2. length of complex vector > 4
    while (xx + yy <= escapeRadius && i < maxIterations)
    {
        i++;
        xy = x*y;
        xx = x*x;
        yy = y*y;
        x = xx - yy + a;
        y = 2*xy + b;
    }
    var w = smoothColor(i,xx,yy);
    return hsvToRgb(360*w/maxIterations,1.0,1.0);

}

function drawMandelbrot(e) {
    var startTime = millis();


    //give e object variables meaning...
    var img = e.data.img;
    var pixels = img.data;
    var numIterations = e.data.numIterations;
    var width = e.data.width;
    var height = e.data.height;
    var zoom = e.data.zoom;
    var xoffset = e.data.xoffset;
    var yoffset = e.data.yoffset;
    var escapeRadius = e.data.escape;
    var color = e.data.color;
    //

    var amin = -1.5*(width/height);
    var amax = 1.5*(width/height);
    var bmin = -1.5;
    var bmax = 1.5;

    var offset = 0;

    for (var py = 0; py < height; ++py) {
        for (var px = 0; px < width; ++px) {
            //map real pixel to complex plane
            var a = px * (amax - amin) / (width - 1) + amin;
            var b = py * (bmax - bmin) / (height - 1) + bmin;
            //transform for pan & zoom
            a = (1.0/zoom)*a + (amax/width)*xoffset;
            b = (1.0/zoom)*b + (bmax/height)*yoffset;

            var i = coreIterator(a, b, numIterations,escapeRadius);

            //pixel data is a one dimensional array :)
            //pixel location within array.
            //each pixel has 4 elements (RGBA)
             //we alter Red, Green, Blue values of pixels but leave alpha constant.


            switch (color){
                case "red":
                    pixels[offset++] = i.r;
                    pixels[offset++] = i.g;
                    pixels[offset++] = i.b;
                    break;
                case "blue":
                    pixels[offset++] = i.b;
                    pixels[offset++] = i.g;
                    pixels[offset++] = i.r;
                    break;
                case "green":
                    pixels[offset++] = i.g;
                    pixels[offset++] = i.r;
                    pixels[offset++] = i.b;
                    break;
                case "purple":
                    pixels[offset++] = Math.sqrt(i.r);
                    pixels[offset++] = i.b ;
                    pixels[offset++] = i.g;
                    break;
                case "grayscale":
                    var grayscale = i.r * 0.3 + i.g * 0.59 + i.b * 0.11;
                    pixels[offset++] = grayscale;
                    pixels[offset++] = grayscale;
                    pixels[offset++] = grayscale;
                    break;
            }

            pixels[offset++] = 255;


        }
    }
    var runTimeInSeconds = (millis() - startTime)/1000;

    //complete madelbrot job results and stats!
    return {
        runTimeInSeconds    :   runTimeInSeconds,
        img                 :   img,
        numIterations       :   numIterations,
        workerId            :   e.data.workerId,
        type                :   'render'
    };


}

//convert hsv color to rgb color. Thanks wikipedia!
function hsvToRgb(h, s, v)
{
    if ( v > 1.0 ) v = 1.0;
    var hp = h/60.0;
    var c = v * s;
    var x = c*(1 - Math.abs((hp % 2) - 1));
    var rgba = {
        r : 0,
        g : 0,
        b : 0,
        a : 255
    };

    if ( 0<=hp && hp<1 ){rgba.r = c; rgba.g = x;}
    if ( 1<=hp && hp<2 ){rgba.r = x; rgba.g = c;}
    if ( 2<=hp && hp<3 ){rgba.g = c; rgba.b = x;}
    if ( 3<=hp && hp<4 ){rgba.g = x; rgba.b = c;}
    if ( 4<=hp && hp<5 ){rgba.r = x; rgba.b = c;}
    if ( 5<=hp && hp<6 ){rgba.r = c; rgba.b = x;}

    var m = v - c;

    rgba.r = (rgba.r + m) * 255;
    rgba.g = (rgba.g + m) * 255;
    rgba.b = (rgba.b + m) * 255;

    return rgba;
}

//use common 'smooth' color algoritm for mandelbrot
//otherwise banding appears
function smoothColor(iterations,x,y)
{
    return 1 + iterations - Math.log(Math.log(Math.sqrt(x*x + y*y)))/Math.log(2.0);
}

function millis()
{
    var d = new Date();
    return d.getTime();
}

function debug(msg){
    self.postMessage({type : 'debug', msg : msg});
}

//main listener. Listens to main thread for task, completes task and responds with results.
// Workers have no access to document object thus worker just returns array of pixels, and does not display them
self.addEventListener('message',function(e){
    //console.log("Mandel-> a: ("+ e.data.amin + "," + e.data.amax + ")  b: (" + e.data.bmin + "," + e.data.bmax + ")\n");
    var mandelJob = drawMandelbrot(e);
    self.postMessage(mandelJob);
},false);









